# Digitalent

## Contenu du projet 
1. app : code source de l'application (Symfony 4)
2. nginx : configuration NGINX + image Docker (Utile si vous utilisez Docker) 
3. php-fpm : configuration PHP-FPM + image Docker (Utile si vous utilisez Docker)
4. docker-compose.yml : Si vous utilisez Docker, utile pour lancer l'installation de tout l'environnement.

## Environnement requis

- Serveur Apache ou NGINX
- PHP
- Accès à un serveur MySQL
- Composer : https://getcomposer.org/

### Si vous utilisez Docker
- docker v18+
- docker-compose v2+

## Script utiles

```
php bin/console make:entity # Créé une nouvelle entitée

php bin/console make:migration # Créé un nouveau fichier de migration à partir d'une entitée, utile pour mettre a jour la base de données

php bin/console doctrine:migration:migrate # Execute les scripts de migration

```

## Utile

Ajouter un flash message (depuis le controller): 
```php
$this->addFlash(
    AlertTypeEnum::INFO,
    'Pas de recherche trouvées. Veuillez rééssayer'
);
```

**Pas besoin d'éditer les vues !**