<?php

namespace App\Controller;

use App\Entity\DMail;
use App\Entity\User;
use App\Enum\AlertTypeEnum;
use App\Form\DMailType;
use App\Repository\DMailRepository;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManager;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Route("/dmail")
 */
class DMailController extends AbstractController
{
    /**
     * @Route("/", name="d_mail_index", methods="GET")
     */
    public function index(Request $request, DMailRepository $dMailRepository): Response
    {
        $connectedUser = $this->getUser();

        $selectedDMail = null;
        $allMails = $dMailRepository->findByUser($connectedUser);

        if (!empty($allMails)) {
            $selectedDMail = $allMails[0] ?? null;

            if (!empty($request->get('d'))) {
                $selectedDMail = $dMailRepository->find($request->get('d'));
                if (is_null($selectedDMail)) {
                    $this->addFlash(AlertTypeEnum::ERROR, "D-mail not found");
                } else {
                    if ($selectedDMail->getRecipient() != $this->getUser()) { // check if connected user has right to read this d-mail
                        $this->addFlash(AlertTypeEnum::ERROR, "You can't read this d-mail");
                        // $selectedDMail = $allMails[0]; // TODO : uncomment after auth implemented
                    }
                }
            }
        } else {
            $this->addFlash(AlertTypeEnum::WARNING, "page.dmail.error.no_messages");
        }
        return $this->render('d_mail/index.html.twig', [
            'd_mails' => $allMails,
            'selectedDMail' => $selectedDMail
        ]);
    }

    /**
     * @Route("/new", name="d_mail_new", methods="GET|POST")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $em = $this->getDoctrine()->getManager();

        $dMail = new DMail();
        if ($request->get('recipient')) {
            $user = $em->getRepository(User::class)->find($request->get('recipient'));
            if (!empty($user)) {
                $dMail->setRecipient($user);
            }
        }

        $form = $this->createForm(DMailType::class, $dMail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // TODO: Remove fake author and replace it with the next line
            $connectedUser = $this->getUser();

            // Remove that line
            $connectedUser = $em->getRepository(User::class)->find(1);

            $dMail->setAuthor($connectedUser);

            self::send($em, $fileUploader, $dMail);

            return $this->redirectToRoute('d_mail_index');
        }

        return $this->render('d_mail/new.html.twig', [
            'd_mail' => $dMail,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="d_mail_delete", methods="DELETE")
     */
    public function delete(Request $request, DMail $dMail) {
        if ($this->isCsrfTokenValid('delete'.$dMail->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dMail);
            $em->flush();
        }

        return $this->redirectToRoute('d_mail_index');
    }

    public static function send(EntityManager $em, FileUploader $fileUploader = null, DMail $dMail)
    {
        if (!empty($fileUploader)) {
            foreach ($dMail->getAttachments() as $attachment) {
                /** @var UploadedFile $file */
                $file = $attachment->getFileURL();

                $attachment->setDMail($dMail);
                $attachment->setName($file->getClientOriginalName());

                $fileName = $fileUploader->upload($file);
                $attachment->setFileURL($fileName);
                $dMail->addAttachment($attachment);

                $em->persist($attachment);
            };
        }

        $em->persist($dMail);
        $em->flush();
    }
}
