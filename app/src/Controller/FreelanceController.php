<?php

namespace App\Controller;

use App\Entity\Freelance;
use App\Entity\Quotation;
use App\Enum\AlertTypeEnum;
use App\Enum\SearchTypeEnum;
use App\Form\FreelanceType;
use App\Form\SearchType;
use App\Repository\FreelanceRepository;
use Proxies\__CG__\App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/freelance")
 */
class FreelanceController extends AbstractController
{

    /**
     * @Route("/", name="freelance_index", methods={"GET"})
     */
    public function searchFreelances(Request $request, FreelanceRepository $freelanceRepository) {

        $search = "";

        if (!empty($request->query->get('search'))) {
            $search = $request->query->get('search');
        }

        $form = $this->createSearchForm($search, SearchTypeEnum::SEARCH_FREELANCE);

        $freelances = $freelanceRepository->findAll(); // replace with search

        if (count($freelances) == 0) {
            $this->addFlash(
                AlertTypeEnum::INFO,
                'Pas de freelances trouvés avec ces critères. Veuillez rééssayer'
            );
            $freelances = $freelanceRepository->findAll();
        }

        return $this->render('freelance/index.html.twig', [
            'freelances'  => $freelances,
            'form'      => $form->createView()
        ]);
    }

    private function createSearchForm($defaultValue = "", $defaultChoice = "") {
        return $this->createForm(SearchType::class, null, [
            'action'            => $this->generateUrl('search'),
            'searchValue'       => $defaultValue,
            'choiceValue'       => $defaultChoice
        ]);
    }

    /**
     * @Route("/new", name="freelance_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $freelance = new Freelance();
        $form = $this->createForm(FreelanceType::class, $freelance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($freelance);
            $em->flush();

            return $this->redirectToRoute('freelance_index');
        }

        return $this->render('freelance/new.html.twig', [
            'freelance' => $freelance,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/dashboard", name="freelance_dashboard", methods="GET")
     * @param Request $request
     */
    public function dashboard(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $connectedUser = $this->getUser();

        $freelance = $em->getRepository(Freelance::class)->findOneBy(['user'    => $connectedUser]);

        $quotations = $em->getRepository(Quotation::class)->findAllQuotationsFromFreelance($freelance);

        if (empty($freelance)) {
            throw new NotFoundHttpException("This user is not registered as Freelance.");
        }

        return $this->render('freelance/dashboard.html.twig', [
            'freelance'     => $freelance,
            'quotations'    => $quotations
        ]);
    }

    /**
     * @Route("/{id}", name="freelance_show", methods="GET")
     */
    public function show(Freelance $freelance): Response
    {
        return $this->render('freelance/show.html.twig', ['freelance' => $freelance]);
    }

    /**
     * @Route("/{id}/edit", name="freelance_edit", methods="GET|POST")
     */
    public function edit(Request $request, Freelance $freelance): Response
    {
        $form = $this->createForm(FreelanceType::class, $freelance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('freelance_index', ['id' => $freelance->getId()]);
        }

        return $this->render('freelance/edit.html.twig', [
            'freelance' => $freelance,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="freelance_delete", methods="DELETE")
     */
    public function delete(Request $request, Freelance $freelance): Response
    {
        if ($this->isCsrfTokenValid('delete'.$freelance->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($freelance);
            $em->flush();
        }

        return $this->redirectToRoute('freelance_index');
    }
}
