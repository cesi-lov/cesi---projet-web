<?php

namespace App\Controller;

use App\Entity\Freelance;
use App\Entity\Project;
use App\Form\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $searchForm = $this->createForm(SearchType::class, null, [
            'action'    => $this->generateUrl('search')
        ]);

        // Get Projects
        $em = $this->getDoctrine()->getManager();
        $projects = $em->getRepository(Project::class)->findLatest(3);

        // Get freelances
        $freelances = $em->getRepository(Freelance::class)->findLatest(3);

        return $this->render('home/index.html.twig', [
            'controller_name'   => 'HomeController',
            'projects'          => $projects,
            'freelances'        => $freelances,
            'form'              => $searchForm->createView()
        ]);
    }
}
