<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\Deliverable;
use App\Entity\DMail;
use App\Entity\Evaluation;
use App\Entity\Freelance;
use App\Entity\Project;
use App\Entity\User;
use App\Enum\AlertTypeEnum;
use App\Enum\DeliverableStateEnum;
use App\Form\AttachmentType;
use App\Form\ProposeProjectType;
use App\Form\SearchType;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Service\FileUploader;
use PhpParser\Node\Scalar\MagicConst\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/project")
 */
class ProjectController extends AbstractController
{
    /**
     * @Route("/new", name="project_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('search_projects');
        }

        return $this->render('project/new.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_show", methods="GET")
     */
    public function show(Project $project): Response
    {
        // Get Propose Project Form
        $proposeProjectForm = $this->createForm(ProposeProjectType::class, null, [
            'action' => $this->generateUrl('project_proposal', [
                'id' => $project->getId()
            ])
        ]);

        return $this->render('project/show.html.twig', [
            'project' => $project,
            'proposeProjectForm' => $proposeProjectForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="project_edit", methods="GET|POST")
     */
    public function edit(Request $request, Project $project): Response
    {
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(AlertTypeEnum::SUCCESS, "Le projet a bien été modifié");

            //return $this->redirectToRoute('search_projects', ['id' => $project->getId()]);
        }

        return $this->render('project/edit.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_delete", methods="DELETE")
     */
    public function delete(Request $request, Project $project): Response
    {
        if ($this->isCsrfTokenValid('delete' . $project->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($project);
            $em->flush();
        }

        return $this->redirectToRoute('search_projects');
    }

    /**
     * @Route("/{id}/proposeToFreelance", name="project_proposal", methods="POST")
     */
    public function proposeToFreelance(Request $request, TranslatorInterface $translator, Project $project): Response
    {
        $form = $this->createForm(ProposeProjectType::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Freelance $freelance */
            $freelance = $form->getData()['freelanceToPropose'];

            $project->addFreelanceProposal($freelance);
            $freelance->addProposedProject($project);

            $em->persist($project);
            $em->persist($freelance);

            $em->flush();

            $dMail = new DMail();
            $dMail
                ->setAuthor($project->getOwner()->getUser())
                ->setRecipient($freelance->getUser())
                ->setContent($translator->trans('page.project.proposemodal.dmail.default_message', [
                    '%projectName%' => $project->getTitle()
                ]))
                ->setSubject($translator->trans('page.project.proposemodal.dmail.default_subject', [
                    '%name%' => $freelance->getUser()->getUsername(),
                    '%url%' => $this->generateUrl('project_show', ['id' => $project->getId()])
                ]))
                ->setAssociatedProject($project);
            DMailController::send($em, null, $dMail);

            $this->addFlash(AlertTypeEnum::SUCCESS, $translator->trans("page.project.proposemodal.success"));
        }

        return $this->redirectToRoute('project_show', ['id' => $project->getId()]);
    }

    /**
     * @Route("/{id}/dashboard", name="project_dashboard", methods="GET|POST")
     */
    public function dashboard(Request $request, Project $project): Response
    {
        $attachmentForm = $this->createForm(AttachmentType::class, null, [
            'action' => $this->generateUrl('deliverable_propose', ['id' => $project->getId()])
        ]);

        return $this->render('project/dashboard.html.twig', [
            'project' => $project,
            'attachment_form' => $attachmentForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/changeDeliverableState/{state}", name="deliverable_change_state", methods="POST")
     */
    public function changeState(Request $request, Project $project, String $state): Response
    {
        if ($this->isCsrfTokenValid('change_state', $request->request->get('_token'))) {
            $idDeliverable = $request->request->get('idDeliverable');
            if (empty ($idDeliverable)) {
                throw new ParameterNotFoundException("idDeliverable is empty");
            }

            if (!DeliverableStateEnum::isValidValue($state)) {
                throw new ParameterNotFoundException("State is not in DeliverableStateEnum");
            }

            $em = $this->getDoctrine()->getManager();
            /** @var Deliverable $deliverable */
            $deliverable = $em->getRepository(Deliverable::class)->find($idDeliverable);
            $deliverable->setState($state);
            $em->persist($deliverable);
            $em->flush();

            return $this->redirectToRoute('project_dashboard', ['id' => $project->getId()]);
        }
    }


    /**
     * @Route("/{id}/rateFreelance", name="rate_freelance", methods="POST")
     */
    public function rateFreelance(Request $request, Project $project): Response
    {
        if ($this->isCsrfTokenValid('rate_freelance', $request->request->get('_token'))) {
            $idProjectOwner = $request->request->get('idProjectOwner');
            $idFreelance = $request->request->get('idFreelance');
            $rate = $request->request->get('idRate');
            if (empty ($rate)) {
                throw new ParameterNotFoundException("idRate is empty");
            }

            if ($rate > 5 || $rate < 0) {
                throw new InvalidParameterException("Rate is not between 0 and 5");
            }

            $em = $this->getDoctrine()->getManager();
            $evaluation = $em->getRepository(Evaluation::class)->findByPOFreelanceProject(
                $idProjectOwner,
                $idFreelance,
                $project);
            if (empty($evaluation)) {
                $evaluation = new Evaluation();
                $projectOwner = $em->getRepository(User::class)->find($idProjectOwner);
                $freelance = $em->getRepository(User::class)->find($idFreelance);
                $evaluation
                    ->setRater($projectOwner)
                    ->setRated($freelance)
                    ->setProject($project)
                    ->setNote($rate)
                    ->setComment("");
            } else {
                throw new InvalidParameterException("You cannot rate twice the same user for the same project");
            }
            $em->persist($evaluation);
            $em->flush();
        }
        return $this->redirectToRoute('project_dashboard', ['id' => $project->getId()]);

    }

    /**
     * @Route("/{id}/proposeDeliverable", name="deliverable_propose", methods="POST")
     */
    public function proposeDeliverable(Request $request, FileUploader $fileUploader, Project $project): Response
    {
        $attachment = new Attachment();
        $form = $this->createForm(AttachmentType::class, $attachment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $idDeliverable = $request->request->get('idDeliverable');
            if (empty ($idDeliverable)) {
                throw new ParameterNotFoundException("idDeliverable is empty");
            }

            $file = $attachment->getFileURL();
            $fileName = $fileUploader->upload($file);
            $attachment->setName($fileName);

            $em = $this->getDoctrine()->getManager();
            /** @var Deliverable $deliverable */
            $deliverable = $em->getRepository(Deliverable::class)->find($idDeliverable);
            $deliverable->setAttachment($attachment);
            $deliverable->setState(DeliverableStateEnum::AWAITING_VALIDATION);
            $em->persist($deliverable);
            $em->persist($attachment);
            $em->flush();

            return $this->redirectToRoute('project_dashboard', ['id' => $project->getId()]);
        }
    }

    /**
     * @Route("/{id}/deliverable/ajax", name="deliverable_ajax_show", methods="GET")
     */
    public function ajaxGetDeliverable(Request $request, Project $project): Response
    {
        $deliverableId = $request->query->get('deliverableId');

        $em = $this->getDoctrine()->getManager();
        $deliverable = $em->getRepository(Deliverable::class)->find($deliverableId);

        if ($deliverable->getQuotation()->getProject() != $project) {
            return $this->json([]);
        }
        return $this->json($deliverable);
    }

    /**
     * @Route("/{id}/validate", name="deliverable_validate", methods="POST")
     */
    public function validateDeliverable(Request $request, Project $project): Response
    {
        if ($this->isCsrfTokenValid('validate' . $project->getId(), $request->request->get('_token'))) {
            $idDeliverable = $request->request->get('idDeliverable');
            if (empty ($idDeliverable)) {
                throw new ParameterNotFoundException("idDeliverable is empty");
            }

            $em = $this->getDoctrine()->getManager();
            $deliverable = $em->getRepository(Deliverable::class)->find($idDeliverable);
            if (empty($deliverable)) {
                throw new \InvalidArgumentException("Deliverable does not exists");
            }
            return $this->render('project/paiement_deliverable.html.twig', [
                'deliverable' => $deliverable
            ]);
        }
        throw new \InvalidArgumentException("You can't access to this page");
    }

    /**
     * @Route("/{id}/{idDeliverable}/validatePaiement", name="validatePaiement", methods="POST")
     */
    public function paiementValidate(Request $request, Project $project, Deliverable $deliverable): Response
    {
        return $this->render('project/paiement_deliverable.html.twig', [
            'deliverable' => $deliverable
        ]);
    }
}
