<?php

namespace App\Controller;

use App\Entity\ProjectOwner;
use App\Form\ProjectOwnerType;
use App\Repository\ProjectOwnerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/project_owner")
 */
class ProjectOwnerController extends AbstractController
{
    /**
     * @Route("/", name="project_owner_index", methods="GET")
     */
    public function index(ProjectOwnerRepository $projectOwnerRepository): Response
    {
        return $this->render('project_owner/index.html.twig', ['project_owners' => $projectOwnerRepository->findAll()]);
    }

    /**
     * @Route("/new", name="project_owner_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $projectOwner = new ProjectOwner();
        $form = $this->createForm(ProjectOwnerType::class, $projectOwner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectOwner);
            $em->flush();

            return $this->redirectToRoute('project_owner_index');
        }

        return $this->render('project_owner/new.html.twig', [
            'project_owner' => $projectOwner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_owner_show", methods="GET")
     */
    public function show(ProjectOwner $projectOwner): Response
    {
        return $this->render('project_owner/show.html.twig', ['project_owner' => $projectOwner]);
    }

    /**
     * @Route("/{id}/edit", name="project_owner_edit", methods="GET|POST")
     */
    public function edit(Request $request, ProjectOwner $projectOwner): Response
    {
        $form = $this->createForm(ProjectOwnerType::class, $projectOwner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('project_owner_index', ['id' => $projectOwner->getId()]);
        }

        return $this->render('project_owner/edit.html.twig', [
            'project_owner' => $projectOwner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_owner_delete", methods="DELETE")
     */
    public function delete(Request $request, ProjectOwner $projectOwner): Response
    {
        if ($this->isCsrfTokenValid('delete'.$projectOwner->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projectOwner);
            $em->flush();
        }

        return $this->redirectToRoute('project_owner_index');
    }
}
