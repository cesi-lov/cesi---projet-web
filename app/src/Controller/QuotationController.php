<?php

namespace App\Controller;

use App\Entity\Deliverable;
use App\Entity\Freelance;
use App\Entity\Project;
use App\Entity\Quotation;
use App\Enum\AlertTypeEnum;
use App\Enum\ProjectStateEnum;
use App\Enum\QuotationStateEnum;
use App\Form\QuotationType;
use App\Repository\QuotationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/quotation")
 */
class QuotationController extends AbstractController
{
    /**
     * @Route("/", name="quotation_index", methods="GET")
     * @param QuotationRepository $quotationRepository
     * @return Response
     */
    public function index(QuotationRepository $quotationRepository): Response
    {
        return $this->render('quotation/index.html.twig', ['quotations' => $quotationRepository->findAll()]);
    }

    /**
     * @Route("/{id}/new", name="quotation_new", methods="GET|POST")
     * @param Request $request
     * @param Project $project
     * @return Response
     */
    public function new(Request $request, Project $project): Response
    {
        $em = $this->getDoctrine()->getManager();
        $freelance = $em->getRepository(Freelance::class)->findOneBy(['user' => $this->getUser()]);

        $quotation = new Quotation();
        $quotation->setProject($project);
        $quotation->setOwner($freelance);
        $form = $this->createForm(QuotationType::class, $quotation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $deliverables = $quotation->getDeliverables();

            $em->persist($quotation);
            foreach ($deliverables as $deliverable) {
                $deliverable->setQuotation($quotation);
                $em->persist($deliverable);
            }
            $em->flush();

            return $this->redirectToRoute('project_show', ['id' => $project->getId()]);
        }

        return $this->render('quotation/new.html.twig', [
            'quotation' => $quotation,
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="quotation_show", methods="GET")
     * @param Quotation $quotation
     * @return Response
     */
    public function show(Quotation $quotation): Response
    {
//        TODO ajouter contrôle utilisateur PO ou Freelance !=
        return $this->render('quotation/show.html.twig', ['quotation' => $quotation]);
    }

    /**
     * @Route("/{id}", name="quotation_answer", methods="POST")
     * @param Request $request
     * @param Quotation $quotation
     * @return Response
     */
    public function answer(Request $request, Quotation $quotation): Response
    {
        if ($this->isCsrfTokenValid('answer'.$quotation->getId(), $request->request->get('_token'))) {
            $answer = $request->request->get('_answer');
            if ($answer == QuotationStateEnum::REFUSED || $answer == QuotationStateEnum::ACCEPTED) {

                $em = $this->getDoctrine()->getManager();
                $quotation->setState($answer);
                $quotation->getProject()->setAcceptedQuotation($quotation);
                $em->persist($quotation);
                $em->persist($quotation->getProject());
                $em->flush();
            } else {
                $this->addFlash(
                    AlertTypeEnum::WARNING,
                    'This quotation state is not valid. Choices are accepted or refused.'
                );
            }

        }

        return $this->redirectToRoute('quotation_index');
    }

    /**
     * @Route("/{id}", name="quotation_delete", methods="DELETE")
     * @param Request $request
     * @param Quotation $quotation
     * @return Response
     */
    public function delete(Request $request, Quotation $quotation): Response
    {
        if ($this->isCsrfTokenValid('delete' . $quotation->getId(), $request->request->get('_token'))) {
            if ($quotation->getState() == QuotationStateEnum::REFUSED ||
                $quotation->getProject()->getState() == ProjectStateEnum::DONE ||
                $quotation->getProject()->getState() == ProjectStateEnum::WITHDRAWN) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($quotation);
                $em->flush();
            } else {
                $this->addFlash(
                    AlertTypeEnum::WARNING,
                    'Vous ne pouvez pas supprimer un devis dans cet état.'
                );
            }

        }

        return $this->redirectToRoute('quotation_index');
    }

}
