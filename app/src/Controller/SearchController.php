<?php

namespace App\Controller;

use App\Enum\SearchTypeEnum;
use App\Form\SearchType;
use App\Repository\FreelanceRepository;
use App\Repository\ProjectRepository;
use App\Enum\AlertTypeEnum;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * Determine and route to correct search type (project or freelance)
     * @Route("/search", name="search", methods={"POST"})
     */
    public function searchAll(Request $request)
    {
        // Handle form
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Redirect to project or freelance
            if ($form->getData()['choice'] == SearchTypeEnum::SEARCH_PROJECT) {
                return $this->redirectToRoute('search_projects', [
                    'search' => $form->getData()['search']
                ]);
            } else {
                return $this->redirectToRoute('freelance_index', [
                    'search' => $form->getData()['search']
                ]);
            }
        }
        throw new BadRequestHttpException('Form has not been submitted correctly or is invalid.', null, 400);
    }

    /**
     * @Route("/search/projects", name="search_projects", methods={"GET"})
     */
    public function searchProjects(Request $request, ProjectRepository $projectRepository) {

        $search = "";

        if (!empty($request->query->get('search'))) {
            $search = $request->query->get('search');
        }

        $form = $this->createSearchForm($search, SearchTypeEnum::SEARCH_PROJECT);

        $projects = $projectRepository->findAllTitleContains($search);
        if (count($projects) == 0) {
            $this->addFlash(
                AlertTypeEnum::INFO,
                'Pas de missions trouvées avec ces critères. Veuillez rééssayer'
            );
            $projects = $projectRepository->findAll();
        }


        return $this->render('project/index.html.twig', [
            'projects' => $projects,
            'form'  => $form->createView()
        ]);
    }

    private function createSearchForm($defaultValue = "", $defaultChoice = "") {
        return $this->createForm(SearchType::class, null, [
            'action'            => $this->generateUrl('search'),
            'searchValue'       => $defaultValue,
            'choiceValue'       => $defaultChoice
        ]);
    }
}
