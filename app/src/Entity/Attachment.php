<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttachmentRepository")
 */
class Attachment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\File()
     */
    private $fileURL;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DMail", inversedBy="attachments")
     */
    private $dMail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFileURL()
    {
        return $this->fileURL;
    }

    public function setFileURL($fileURL): self
    {
        $this->fileURL = $fileURL;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDMail(): ?DMail
    {
        return $this->dMail;
    }

    public function setDMail(?DMail $dMail): self
    {
        $this->dMail = $dMail;

        return $this;
    }
}
