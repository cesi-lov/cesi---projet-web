<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EvaluationRepository")
 */
class Evaluation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evaluations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rater;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evaluationsReceived")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rated;

    /**
     * @ORM\Column(type="integer")
     */
    private $note;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="evaluations")
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRater(): ?User
    {
        return $this->rater;
    }

    public function setRater(?User $rater): self
    {
        $this->rater = $rater;

        return $this;
    }

    public function getRated(): ?User
    {
        return $this->rated;
    }

    public function setRated(?User $rated): self
    {
        $this->rated = $rated;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}
