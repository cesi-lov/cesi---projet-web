<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FreelanceRepository")
 */
class Freelance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resume;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $experiences;

    /**
     * @OneToOne(targetEntity="User", cascade={"persist", "remove"}, fetch="EAGER")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quotation", mappedBy="owner")
     */
    private $quotations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="freelanceProposals")
     */
    private $proposedProjects;

    public function __construct()
    {
        $this->quotations = new ArrayCollection();
        $this->proposedProjects = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(?string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getExperiences(): ?string
    {
        return $this->experiences;
    }

    public function setExperiences(?string $experiences): self
    {
        $this->experiences = $experiences;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Quotation[]
     */
    public function getQuotations(): Collection
    {
        return $this->quotations;
    }

    public function addQuotation(Quotation $quotation): self
    {
        if (!$this->quotations->contains($quotation)) {
            $this->quotations[] = $quotation;
            $quotation->setOwner($this);
        }

        return $this;
    }

    public function removeQuotation(Quotation $quotation): self
    {
        if ($this->quotations->contains($quotation)) {
            $this->quotations->removeElement($quotation);
            // set the owning side to null (unless already changed)
            if ($quotation->getOwner() === $this) {
                $quotation->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProposedProjects(): Collection
    {
        return $this->proposedProjects;
    }

    public function addProposedProject(Project $proposedProject): self
    {
        if (!$this->proposedProjects->contains($proposedProject)) {
            $this->proposedProjects[] = $proposedProject;
        }

        return $this;
    }

    public function removeProposedProject(Project $proposedProject): self
    {
        if ($this->proposedProjects->contains($proposedProject)) {
            $this->proposedProjects->removeElement($proposedProject);
        }

        return $this;
    }
}
