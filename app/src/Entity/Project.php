<?php

namespace App\Entity;

use App\Enum\ProjectStateEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('in_progress', 'awaiting', 'done', 'withdrawn')")
     */
    private $state = ProjectStateEnum::AWAITING;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tags;

    /**
     * @Assert\GreaterThan(value=0)
     * @ORM\Column(type="float")
     */
    private $budget;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProjectOwner", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quotation", mappedBy="project")
     */
    private $quotations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Freelance", inversedBy="proposedProjects")
     */
    private $freelanceProposals;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Quotation", cascade={"persist", "remove"})
     */
    private $acceptedQuotation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evaluation", mappedBy="project")
     */
    private $evaluations;

    public function __construct()
    {
        $this->quotations = new ArrayCollection();
        $this->freelanceProposals = new ArrayCollection();
        $this->evaluations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(?string $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getBudget(): ?float
    {
        return $this->budget;
    }

    public function setBudget(float $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * @return Collection|Quotation[]
     */
    public function getQuotations(): Collection
    {
        return $this->quotations;
    }

    public function addQuotation(Quotation $quotation): self
    {
        if (!$this->quotations->contains($quotation)) {
            $this->quotations[] = $quotation;
            $quotation->setProject($this);
        }

        return $this;
    }

    public function removeQuotation(Quotation $quotation): self
    {
        if ($this->quotations->contains($quotation)) {
            $this->quotations->removeElement($quotation);
            // set the owning side to null (unless already changed)
            if ($quotation->getProject() === $this) {
                $quotation->setProject(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?ProjectOwner
    {
        return $this->owner;
    }

    public function setOwner(?ProjectOwner $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|Freelance[]
     */
    public function getFreelanceProposals(): Collection
    {
        return $this->freelanceProposals;
    }

    public function addFreelanceProposal(Freelance $freelanceProposal): self
    {
        if (!$this->freelanceProposals->contains($freelanceProposal)) {
            $this->freelanceProposals[] = $freelanceProposal;
        }

        return $this;
    }

    public function removeFreelanceProposal(Freelance $freelanceProposal): self
    {
        if ($this->freelanceProposals->contains($freelanceProposal)) {
            $this->freelanceProposals->removeElement($freelanceProposal);
        }

        return $this;
    }

    public function getAcceptedQuotation(): ?Quotation
    {
        return $this->acceptedQuotation;
    }

    public function setAcceptedQuotation(?Quotation $acceptedQuotation): self
    {
        $this->acceptedQuotation = $acceptedQuotation;

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function addEvaluation(Evaluation $evaluation): self
    {
        if (!$this->evaluations->contains($evaluation)) {
            $this->evaluations[] = $evaluation;
            $evaluation->setProject($this);
        }

        return $this;
    }

    public function removeEvaluation(Evaluation $evaluation): self
    {
        if ($this->evaluations->contains($evaluation)) {
            $this->evaluations->removeElement($evaluation);
            // set the owning side to null (unless already changed)
            if ($evaluation->getProject() === $this) {
                $evaluation->setProject(null);
            }
        }

        return $this;
    }

   }
