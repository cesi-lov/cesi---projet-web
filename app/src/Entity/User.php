<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evaluation", mappedBy="rater")
     */
    private $evaluationsSent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evaluation", mappedBy="rated")
     */
    private $evaluationsReceived;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DMail", mappedBy="author")
     */
    private $dMailsSent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DMail", mappedBy="Recipient")
     */
    private $dMailsRecieved;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255, columnDefinition="ENUM('freelance','project_owner')")
     */
    private $userType;

    public function __construct()
    {
        $this->evaluationsSent = new ArrayCollection();
        $this->evaluationsReceived = new ArrayCollection();
        $this->dMailsSent = new ArrayCollection();
        $this->dMailsRecieved = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluationsSent(): Collection
    {
        return $this->evaluationsSent;
    }

    public function addEvaluationSent(Evaluation $evaluation): self
    {
        if (!$this->evaluationsSent->contains($evaluation)) {
            $this->evaluationsSent[] = $evaluation;
            $evaluation->setRater($this);
        }

        return $this;
    }

    public function removeEvaluationSent(Evaluation $evaluation): self
    {
        if ($this->evaluationsSent->contains($evaluation)) {
            $this->evaluationsSent->removeElement($evaluation);
            // set the owning side to null (unless already changed)
            if ($evaluation->getRater() === $this) {
                $evaluation->setRater(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluationsReceived(): Collection
    {
        return $this->evaluationsReceived;
    }

    public function addEvaluationsReceived(Evaluation $evaluationsReceived): self
    {
        if (!$this->evaluationsReceived->contains($evaluationsReceived)) {
            $this->evaluationsReceived[] = $evaluationsReceived;
            $evaluationsReceived->setRated($this);
        }

        return $this;
    }

    public function removeEvaluationsReceived(Evaluation $evaluationsReceived): self
    {
        if ($this->evaluationsReceived->contains($evaluationsReceived)) {
            $this->evaluationsReceived->removeElement($evaluationsReceived);
            // set the owning side to null (unless already changed)
            if ($evaluationsReceived->getRated() === $this) {
                $evaluationsReceived->setRated(null);
            }
        }

        return $this;
    }

    public function addEvaluationsSent(Evaluation $evaluationsSent): self
    {
        if (!$this->evaluationsSent->contains($evaluationsSent)) {
            $this->evaluationsSent[] = $evaluationsSent;
            $evaluationsSent->setRater($this);
        }

        return $this;
    }

    public function removeEvaluationsSent(Evaluation $evaluationsSent): self
    {
        if ($this->evaluationsSent->contains($evaluationsSent)) {
            $this->evaluationsSent->removeElement($evaluationsSent);
            // set the owning side to null (unless already changed)
            if ($evaluationsSent->getRater() === $this) {
                $evaluationsSent->setRater(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DMail[]
     */
    public function getDMailsSent(): Collection
    {
        return $this->dMailsSent;
    }

    public function addDMailsSent(DMail $dMailsSent): self
    {
        if (!$this->dMailsSent->contains($dMailsSent)) {
            $this->dMailsSent[] = $dMailsSent;
            $dMailsSent->setAuthor($this);
        }

        return $this;
    }

    public function removeDMailsSent(DMail $dMailsSent): self
    {
        if ($this->dMailsSent->contains($dMailsSent)) {
            $this->dMailsSent->removeElement($dMailsSent);
            // set the owning side to null (unless already changed)
            if ($dMailsSent->getAuthor() === $this) {
                $dMailsSent->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DMail[]
     */
    public function getDMailsRecieved(): Collection
    {
        return $this->dMailsRecieved;
    }

    public function addDMailsRecieved(DMail $dMailsRecieved): self
    {
        if (!$this->dMailsRecieved->contains($dMailsRecieved)) {
            $this->dMailsRecieved[] = $dMailsRecieved;
            $dMailsRecieved->setRecipient($this);
        }

        return $this;
    }

    public function removeDMailsRecieved(DMail $dMailsRecieved): self
    {
        if ($this->dMailsRecieved->contains($dMailsRecieved)) {
            $this->dMailsRecieved->removeElement($dMailsRecieved);
            // set the owning side to null (unless already changed)
            if ($dMailsRecieved->getRecipient() === $this) {
                $dMailsRecieved->setRecipient(null);
            }
        }

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return array('ROLE_USER');
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {

        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
        return "salt";
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getuserType(): ?string
    {
        return $this->userType;
    }

    public function setUserType(string $userType): self
    {
        $this->userType = $userType;

        return $this;
    }
}
