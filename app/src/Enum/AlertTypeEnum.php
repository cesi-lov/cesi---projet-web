<?php
/**
 * Created by PhpStorm.
 * User: loick
 * Date: 20/12/18
 * Time: 08:43
 */

namespace App\Enum;


abstract class AlertTypeEnum
{
    const ERROR = "danger";
    const SUCCESS = "success";
    const INFO = "info";
    const WARNING = "warning";
}