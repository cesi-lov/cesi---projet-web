<?php
/**
 * Created by PhpStorm.
 * User: lili
 * Date: 15/01/19
 * Time: 16:32
 */

namespace App\Enum;


abstract class DeliverableStateEnum extends BasicEnum
{
    const ACCEPTED = "accepted";
    const AWAITING = "awaiting";
    const WIP = "wip";
    const AWAITING_VALIDATION = "awaiting_validation";
    const REFUSED = "refused";
}