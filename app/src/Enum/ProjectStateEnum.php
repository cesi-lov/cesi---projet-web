<?php
/**
 * Created by PhpStorm.
 * User: lili
 * Date: 15/01/19
 * Time: 16:32
 */

namespace App\Enum;


abstract class ProjectStateEnum
{
    const IN_PROGRESS = "in_progress";
    const AWAITING = "awaiting";
    const DONE = "done";
    const WITHDRAWN = "withdrawn";
}