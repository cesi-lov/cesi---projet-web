<?php
/**
 * Created by PhpStorm.
 * User: lili
 * Date: 15/01/19
 * Time: 16:32
 */

namespace App\Enum;


abstract class QuotationStateEnum
{
    const ACCEPTED = "accepted";
    const AWAITING = "awaiting";
    const REFUSED = "refused";
}