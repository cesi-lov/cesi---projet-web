<?php
/**
 * Created by PhpStorm.
 * User: loick
 * Date: 19/12/18
 * Time: 14:58
 */

namespace App\Enum;


abstract class SearchTypeEnum
{
    const SEARCH_PROJECT = "project";
    const SEARCH_FREELANCE = "freelance";
}