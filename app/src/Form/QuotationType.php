<?php

namespace App\Form;

use App\Entity\Deliverable;
use App\Entity\Freelance;
use App\Entity\Project;
use App\Entity\Quotation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class QuotationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estimatedHours', IntegerType::class)
            ->add('description')
            ->add('finalPrice', IntegerType::class)
            ->add('deliverables', CollectionType::class, array(
                // each entry in the array will be an "email" field
                'entry_type' => QuotationDeliverableType::class,
                'allow_add' => true,
                'allow_delete' => true
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Quotation::class,
        ]);
    }
}
