<?php

namespace App\Form;

use App\Enum\SearchTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', TextType::class, [
                'attr'  => [
                    'placeholder' => 'Rechercher'
                ],
                'label'     => false,
                'required'  => false,
                'data'   => $options['searchValue']
            ])
            ->add('choice', ChoiceType::class, [
                'choices' => [
                    'Projets' => SearchTypeEnum::SEARCH_PROJECT,
                    'Freelances' => SearchTypeEnum::SEARCH_FREELANCE
                ],
                'label' => false,
                'data'  => $options['choiceValue'],
            ])
            ->add('send', SubmitType::class, [
                'label' => 'Rechercher'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'searchValue'   => "",
            'choiceValue'   => SearchTypeEnum::SEARCH_PROJECT
        ]);
    }
}
