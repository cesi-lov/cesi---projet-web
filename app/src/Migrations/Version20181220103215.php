<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181220103215 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, tags VARCHAR(255) DEFAULT NULL, budget DOUBLE PRECISION NOT NULL, INDEX IDX_2FB3D0EE7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_owner (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, company LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_DC35EFE2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(13) DEFAULT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE freelance (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, resume VARCHAR(255) DEFAULT NULL, experiences LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_48ABC675A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, rater_id INT NOT NULL, rated_id INT NOT NULL, note INT NOT NULL, comment LONGTEXT NOT NULL, INDEX IDX_1323A5753FC1CD0A (rater_id), INDEX IDX_1323A5754AB3C549 (rated_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quotation (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, project_id INT NOT NULL, estimated_hours DOUBLE PRECISION NOT NULL, final_price DOUBLE PRECISION NOT NULL, state ENUM(\'accepted\',\'refused\',\'awaiting\'), INDEX IDX_474A8DB97E3C61F9 (owner_id), INDEX IDX_474A8DB9166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE7E3C61F9 FOREIGN KEY (owner_id) REFERENCES project_owner (id)');
        $this->addSql('ALTER TABLE project_owner ADD CONSTRAINT FK_DC35EFE2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE freelance ADD CONSTRAINT FK_48ABC675A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A5753FC1CD0A FOREIGN KEY (rater_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A5754AB3C549 FOREIGN KEY (rated_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE quotation ADD CONSTRAINT FK_474A8DB97E3C61F9 FOREIGN KEY (owner_id) REFERENCES freelance (id)');
        $this->addSql('ALTER TABLE quotation ADD CONSTRAINT FK_474A8DB9166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quotation DROP FOREIGN KEY FK_474A8DB9166D1F9C');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE7E3C61F9');
        $this->addSql('ALTER TABLE project_owner DROP FOREIGN KEY FK_DC35EFE2A76ED395');
        $this->addSql('ALTER TABLE freelance DROP FOREIGN KEY FK_48ABC675A76ED395');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A5753FC1CD0A');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A5754AB3C549');
        $this->addSql('ALTER TABLE quotation DROP FOREIGN KEY FK_474A8DB97E3C61F9');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_owner');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE freelance');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE quotation');
    }
}
