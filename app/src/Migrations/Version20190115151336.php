<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190115151336 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dmail (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, recipient_id INT NOT NULL, sent_date DATETIME NOT NULL, subject VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, INDEX IDX_DAF255C4F675F31B (author_id), INDEX IDX_DAF255C4E92F8F78 (recipient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dmail ADD CONSTRAINT FK_DAF255C4F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE dmail ADD CONSTRAINT FK_DAF255C4E92F8F78 FOREIGN KEY (recipient_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE quotation CHANGE state state ENUM(\'accepted\',\'refused\',\'awaiting\')');
        $this->addSql('ALTER TABLE attachment ADD d_mail_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE attachment ADD CONSTRAINT FK_795FD9BB3E9B9513 FOREIGN KEY (d_mail_id) REFERENCES dmail (id)');
        $this->addSql('CREATE INDEX IDX_795FD9BB3E9B9513 ON attachment (d_mail_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attachment DROP FOREIGN KEY FK_795FD9BB3E9B9513');
        $this->addSql('DROP TABLE dmail');
        $this->addSql('DROP INDEX IDX_795FD9BB3E9B9513 ON attachment');
        $this->addSql('ALTER TABLE attachment DROP d_mail_id');
        $this->addSql('ALTER TABLE quotation CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
