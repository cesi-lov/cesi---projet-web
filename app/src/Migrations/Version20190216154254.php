<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190216154254 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_freelance (project_id INT NOT NULL, freelance_id INT NOT NULL, INDEX IDX_599783A2166D1F9C (project_id), INDEX IDX_599783A2E8DF656B (freelance_id), PRIMARY KEY(project_id, freelance_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE freelance_project (freelance_id INT NOT NULL, project_id INT NOT NULL, INDEX IDX_BDEF673E8DF656B (freelance_id), INDEX IDX_BDEF673166D1F9C (project_id), PRIMARY KEY(freelance_id, project_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_freelance ADD CONSTRAINT FK_599783A2166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_freelance ADD CONSTRAINT FK_599783A2E8DF656B FOREIGN KEY (freelance_id) REFERENCES freelance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE freelance_project ADD CONSTRAINT FK_BDEF673E8DF656B FOREIGN KEY (freelance_id) REFERENCES freelance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE freelance_project ADD CONSTRAINT FK_BDEF673166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project CHANGE state state ENUM(\'in_progress\', \'awaiting\', \'done\', \'withdrawn\')');
        $this->addSql('ALTER TABLE quotation CHANGE state state ENUM(\'accepted\',\'refused\',\'awaiting\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE project_freelance');
        $this->addSql('DROP TABLE freelance_project');
        $this->addSql('ALTER TABLE project CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE quotation CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
