<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190216174739 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dmail ADD associated_project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dmail ADD CONSTRAINT FK_DAF255C4FDDA3EDD FOREIGN KEY (associated_project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_DAF255C4FDDA3EDD ON dmail (associated_project_id)');
        $this->addSql('ALTER TABLE project CHANGE state state ENUM(\'in_progress\', \'awaiting\', \'done\', \'withdrawn\')');
        $this->addSql('ALTER TABLE quotation CHANGE state state ENUM(\'accepted\',\'refused\',\'awaiting\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dmail DROP FOREIGN KEY FK_DAF255C4FDDA3EDD');
        $this->addSql('DROP INDEX IDX_DAF255C4FDDA3EDD ON dmail');
        $this->addSql('ALTER TABLE dmail DROP associated_project_id');
        $this->addSql('ALTER TABLE project CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE quotation CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
