<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190224092943 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project ADD accepted_quotation_id INT DEFAULT NULL, CHANGE state state ENUM(\'in_progress\', \'awaiting\', \'done\', \'withdrawn\')');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE9C5F8635 FOREIGN KEY (accepted_quotation_id) REFERENCES quotation (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2FB3D0EE9C5F8635 ON project (accepted_quotation_id)');
        $this->addSql('ALTER TABLE quotation CHANGE state state ENUM(\'accepted\',\'refused\',\'awaiting\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE9C5F8635');
        $this->addSql('DROP INDEX UNIQ_2FB3D0EE9C5F8635 ON project');
        $this->addSql('ALTER TABLE project DROP accepted_quotation_id, CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE quotation CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
