<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190303120302 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project CHANGE state state ENUM(\'in_progress\', \'awaiting\', \'done\', \'withdrawn\')');
        $this->addSql('ALTER TABLE deliverable CHANGE state state ENUM(\'accepted\',\'refused\',\'awaiting\', \'wip\', \'awaiting_validation\')');
        $this->addSql('ALTER TABLE quotation CHANGE state state ENUM(\'accepted\',\'refused\',\'awaiting\')');
        $this->addSql('ALTER TABLE user CHANGE user_type user_type ENUM(\'freelance\',\'project_owner\')');
        $this->addSql('ALTER TABLE evaluation ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A575166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_1323A575166D1F9C ON evaluation (project_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE deliverable CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A575166D1F9C');
        $this->addSql('DROP INDEX IDX_1323A575166D1F9C ON evaluation');
        $this->addSql('ALTER TABLE evaluation DROP project_id');
        $this->addSql('ALTER TABLE project CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE quotation CHANGE state state VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE user_type user_type VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
