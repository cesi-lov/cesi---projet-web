<?php

namespace App\Repository;

use App\Entity\DMail;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DMail|null find($id, $lockMode = null, $lockVersion = null)
 * @method DMail|null findOneBy(array $criteria, array $orderBy = null)
 * @method DMail[]    findAll()
 * @method DMail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DMailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DMail::class);
    }

    public function findByUser(User $user, int $offset = 0, int $nbToRetrieve = 10) {
        return $this->createQueryBuilder('d')
            ->andWhere('d.recipient = :user')
            ->setParameter('user', $user)
            ->orderBy('d.sentDate', 'DESC')
            ->setMaxResults($nbToRetrieve)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return DMail[] Returns an array of DMail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DMail
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
